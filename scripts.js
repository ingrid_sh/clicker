var nbClics = 0;
    
const time = 1000; // On déclare une variable constante de 1000ms = 1s

// On déclare un écouteur d'événement sur la zone de jeu pour le CLIC à l'aide de jQuery
$("#gameArea").on("click", clickAndDisplay);

// On déclare un setTimeout pour arrêter le jeu après 3s
setTimeout(gameOver, time);

// 1) On déclare un écouteur d'événement sur le bouton Recommencer
$("#btnRetry").on("click", () => {
    console.log("On a cliqué sur le bouton recommencer");

    // On réinitialise le score à 0 et on l'affiche
    nbClics = 0;
    $('h2 span.rouge').text(nbClics);

    // On masque le bouton Recommencer
    $("#btnRetry").fadeOut();

    // On met de nouveau un écouteur d'événement sur la zone de jeu
    $("#gameArea").on("click", clickAndDisplay);

    // On relance un timer (setTimeout)
    setTimeout(gameOver, time);
});

// ------------- FUNCTIONS ---------------

function clickAndDisplay() {
    // On incrémente la variable qui compte le nb de clics
    nbClics++;
    $('h2 span.rouge').text(nbClics);
}

function gameOver() {
    alert("FIN DU GAME");
    $("#gameArea").off("click"); // On retire l'écouteur d'événement du clic sur la zone de jeu
    $("#btnRetry").fadeIn(); // On affiche le bouton pour recommencer
}